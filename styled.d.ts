import { typography } from 'globalStyles/theme/fonts'
import { colors } from "globalStyles/theme/palette";


declare module 'styled-components' {
    export interface DefaultTheme {
        typography: typeof typography
        colors: typeof colors
    }
}