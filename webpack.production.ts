import { merge } from "webpack-merge";

import config from "./webpack.common";
import TerserPlugin from "terser-webpack-plugin";

const mainConfig = merge(config, {
  mode: "production",
  optimization: {
    minimize: true,
    splitChunks: {
      chunks: "all",
      maxInitialRequests: Infinity,
      minSize: 0,
      maxSize: 200000, // Set the maximum size limit to 200 KB
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name(module) {
            const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];
            return `vendor_${packageName.replace("@", "")}`;
          },
        },
      },
    },
  },
  performance: {
    maxAssetSize: 200000, // Set maximum asset size limit (200 KB in this example)
  },
  entry: {
    main: "index.tsx",
  },
});

export default mainConfig;
