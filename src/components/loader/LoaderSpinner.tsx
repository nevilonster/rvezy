import styled from "styled-components";

const LoaderSpinner = () => {
  return (
    <div>
      <AnimLoader>
        <span className={"loader"} />
      </AnimLoader>
    </div>
  );
};

export default LoaderSpinner;

const AnimLoader = styled.div`
  .loader {
    width: 48px;
    height: 48px;
    border: 5px solid;
    border-color: #5575d0 transparent;
    border-radius: 50%;
    display: inline-block;
    box-sizing: border-box;
    animation: rotation 1s linear infinite;
  }

  @keyframes rotation {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;
