import { cssTransition, ToastContainer } from "react-toastify";
import styled, { css } from "styled-components";
import cn from "classnames";

import "react-toastify/dist/ReactToastify.css";
const isStickyBottomButton = false;
const isMobile = false;
const ToastMessages = () => {
  const verticalPosition = isMobile ? "bottom" : "top";

  return (
    <StyledToastContainer
      enableMultiContainer={true}
      className={cn("right", { "sticky-offset": isStickyBottomButton && isMobile })}
      position={`${verticalPosition}-right`}
      icon={false}
      draggable={false}
      style={{
        marginRight: "1rem",
        marginTop: "18px",
      }}
    />
  );
};

export default ToastMessages;

export const fadeitoutTransition = cssTransition({
  enter: "fadeInAnim",
  exit: "fadeOutAnim",
});

export const StyledToastContainer = styled(ToastContainer)`
  button {
    min-width: 11px;
  }

  @keyframes fadeIn {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }

  @keyframes fadeOut {
    0% {
      opacity: 1;
    }
    100% {
      opacity: 0;
    }
  }

  .fadeInAnim {
    animation: fadeIn 0.5s ease-in both;
  }

  .fadeOutAnim {
    animation: fadeOut 0.3s ease-out both;
  }

  .Toastify__toast {
    padding: 12px 6px 10px 8px;
    min-height: unset;
    width: 316px;
    margin-bottom: 8px;

    .Toastify__toast-body {
      margin: 0;
      padding: 0;
    }

    .Toastify__close-button {
      position: absolute;
      top: 5px;
      right: 5px;
      color: ${({ theme }) => theme.colors.toast.textColor};
    }

    .Toastify__progress-bar {
      height: 3px;
    }

    .toast-message {
      color: ${({ theme }) => theme.colors.toast.textColor};
      font-family: "Mulish", serif;
      font-weight: normal;
      font-size: 13px;
      line-height: 18px;
      margin: 0 15px 0 11px;
    }

    .toast-icon {
      font-size: 32px;
    }

    .toast-icon-notification {
      margin-top: 2px;
      width: 24px;
      height: 24px;
      min-width: 24px;
      min-height: 24px;
      display: flex;
      border-radius: 12px;
      align-items: center;
      justify-content: center;
    }

    .toast-inner-wrap {
      display: flex;
      align-items: flex-start;
    }

    .toast-notification-wrap {
      display: flex;
      flex-direction: row;
      align-items: flex-start;
    }

    .toast-notification-message-wrap {
      display: flex;
      flex-direction: column;
      margin-left: 10px;
    }

    .toast-notification-header {
      font-size: 13px;
      font-weight: bold;
      font-family: "Mulish", sans-serif;
    }

    .toast-notification-message {
      margin-top: 4px;
      margin-right: 30px;
      font-size: 12px;
      line-height: 18px;
      font-weight: bold;
      font-family: "Mulish", sans-serif;
      max-width: 230px;
      overflow: hidden;
    }

    &--success,
    &--info {
      ${({ theme }) =>
        colorSchemeCSS(theme.colors.toast.infoMain, theme.colors.toast.infoBackground)}
    }

    &--warning {
      ${({ theme }) =>
        colorSchemeCSS(theme.colors.toast.warningMain, theme.colors.toast.warningBackground)}
    }

    &--error {
      ${({ theme }) =>
        colorSchemeCSS(theme.colors.toast.errorMain, theme.colors.toast.errorBackground)}
    }
  }
`;

const colorSchemeCSS = (mainColor, backgroundColor) => css`
  border: 1px solid ${mainColor};
  background-color: ${backgroundColor};

  .Toastify__progress-bar {
    background-color: ${mainColor};
  }

  .toast-icon {
    color: ${mainColor};
  }
`;
