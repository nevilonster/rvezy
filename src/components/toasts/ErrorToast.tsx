import AttentionSVG from "components/toasts/AttentionSVG";

type ComponentProps = {
  header?: string;
  description: string;
};

const ErrorToast = ({ header, description }: ComponentProps) => {
  return (
    <div className="toast-notification-wrap">
      <div
        className={"toast-icon-notification"}
        style={{
          background: "rgba(222, 14, 16, 0.1)",
          color: "rgba(222, 14, 16, 1)",
        }}
      >
        <AttentionSVG color={"rgba(222, 14, 16, 1)"} />
      </div>
      <div className={"toast-notification-message-wrap"}>
        <div className={"toast-notification-header"} style={{ color: "rgba(222, 14, 16, 1)" }}>
          {header ? header : "Attention"}
        </div>
        <div className="toast-notification-message">{description}</div>
      </div>
    </div>
  );
};

export default ErrorToast;
