import React, { lazy, Suspense } from "react";
import { ImageComponentProps } from "components/image/ImageComponent";
const LazyImage = lazy(() => import("./ImageComponent"));

const LazyImageComponent = ({ src, alt }: ImageComponentProps) => {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <LazyImage src={src} alt={alt} />
    </Suspense>
  );
};

export default LazyImageComponent;
