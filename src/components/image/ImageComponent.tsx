import React, { useState, useEffect } from "react";
import styled from "styled-components";
import LoaderSpinner from "components/loader/LoaderSpinner";

export type ImageComponentProps = {
  src: string;
  alt: string;
};
const ImageComponent = ({ src, alt }: ImageComponentProps) => {
  const [isImageLoaded, setIsImageLoaded] = useState(false);

  useEffect(() => {
    const image = new Image();
    image.onload = () => {
      setIsImageLoaded(true);
    };
    image.src = src;
  }, [src]);

  return (
    <Wrap>
      {isImageLoaded ? (
        <RVImage src={src} alt={alt} />
      ) : (
        <LoaderSpinnerWrap>
          <LoaderSpinner />
        </LoaderSpinnerWrap>
      )}
    </Wrap>
  );
};

export default ImageComponent;

const Wrap = styled.div`
  display: flex;
  position: relative;
  width: 300px;
  height: 200px;
  cursor: pointer;
  :hover {
    opacity: 0.8;
  }
`;

const LoaderSpinnerWrap = styled.div`
  position: absolute;
  display: flex;
  left: 120px;
  top: 75px;
`;

const RVImage = styled.img`
  width: 300px;
  height: 200px;
  border: 1px solid gray;
  border-radius: 8px;
  background: whitesmoke;
`;
