import styled from "styled-components";
import { typography } from "globalStyles/theme/fonts";

type ComponentProps = {
  className?: string;
  lat: number;
  lng: number;
  markerId: string | number;
  label?: string;
  onClick?: (
    e: React.MouseEvent<HTMLDivElement, MouseEvent>,
    marker: { markerId: string | number; lat: number; lng: number }
  ) => void;
};

const Marker = ({ className, lat, lng, markerId, label, onClick, ...props }: ComponentProps) => {
  return (
    <ImgWrap
      className={className}
      lat={lat}
      lng={lng}
      onClick={e => (onClick ? onClick(e, { markerId, lat, lng }) : null)}
      style={{ cursor: "pointer", fontSize: 40 }}
      {...props}
    >
      <TextWrap>{label}</TextWrap>
    </ImgWrap>
  );
};

export default Marker;

interface IImgWrap {
  lat: number;
  lng: number;
}
const ImgWrap = styled.div<IImgWrap>`
  width: 40px;
  height: 20px;
  border: 1px solid black;
  border-radius: 4px;
  background-color: red;
  display: block;
  position: relative;
`;

const TextWrap = styled.div`
  ${typography.superSmallText}
  position: absolute;
  overflow: hidden;
  text-overflow: ellipsis;
  max-height: 20px;
`;
