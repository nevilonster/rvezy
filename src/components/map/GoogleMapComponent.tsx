import React, { useMemo, useRef } from "react";
import { IRV } from "model/data/types";

import useRV from "model/context/useRV";
import GoogleMap from "google-maps-react-markers";
import Marker from "components/map/Marker";

type GoogleMapComponentProps = {
  data: IRV[];
};

const GoogleMapComponent: React.FC<GoogleMapComponentProps> = ({ data }) => {
  const mapRef = useRef(null);
  const rvModel = useRV();

  const curData = useMemo(() => {
    return data.map(irv => {
      return { lat: irv.Latitude, lng: irv.Longitude, text: irv.RVName, id: irv.Id };
    });
  }, [data]);

  const onGoogleApiLoaded = ({ map, maps }) => {
    mapRef.current = map;
  };

  const onMapChange = ({ center }) => {
    console.log("bounds", center);
    rvModel.setMapPosition(center[0], center[1]);
  };

  return (
    <GoogleMap
      apiKey={process.env.REACT_APP_GOOGLE_MAP_API_KEY}
      defaultZoom={10}
      defaultCenter={rvModel.mapPosition}
      mapMinHeight="600px"
      onGoogleApiLoaded={onGoogleApiLoaded}
      onChange={onMapChange}
    >
      {curData.map(rv => (
        <Marker lng={rv.lng} lat={rv.lat} key={rv.id} markerId={rv.id} label={rv.text} />
      ))}
    </GoogleMap>
  );
};

export default GoogleMapComponent;
