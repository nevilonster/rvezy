import styled from "styled-components";
import ListRVS from "model/data/ListRVS";
import RVItem from "components/rvs/RVItem";
import { useMemo, useRef, useState } from "react";
import GoogleMapComponent from "components/map/GoogleMapComponent";

type ComponentProps = {
  rvsList: ListRVS;
};

const DEFAULT_HEIGHT_ITEM = 250;
const DEFAULT_COUNT_ITEM = 15;
const RVSComponent = ({ rvsList }: ComponentProps) => {
  const [count, setCount] = useState(DEFAULT_COUNT_ITEM);
  const ref = useRef<HTMLDivElement>();
  const handleScroll = () => {
    const scrollTop = ref.current ? ref.current.scrollTop : 0;
    const newCount = Math.round(scrollTop / DEFAULT_HEIGHT_ITEM) * 5 + DEFAULT_COUNT_ITEM;
    if (newCount > count) {
      setCount(newCount);
    }
  };
  const items = useMemo(() => {
    return rvsList.listRVs.slice(0, count);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [count]);

  return (
    <Wrap onScroll={handleScroll} ref={ref}>
      <MapWrap>
        <GoogleMapComponent data={items} />
      </MapWrap>
      <ItemsWrap>
        {items.map(rv => {
          return <RVItem rv={rv} key={rv.Id} />;
        })}
      </ItemsWrap>
    </Wrap>
  );
};

export default RVSComponent;

const Wrap = styled.div`
  display: flex;
  flex-direction: column;
  overflow-y: scroll;
`;

const ItemsWrap = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  gap: 1rem;
  width: 60%;
`;

const MapWrap = styled.div`
  position: absolute;
  top: 150px;
  right: 45px;
  width: 35%;
  height: 50vh;
`;
