import styled from "styled-components";
import { IRV } from "model/data/types";
import { typography } from "globalStyles/theme/fonts";
import { IMAGE_CDN } from "router/enums";
import { useMemo } from "react";
import LazyImageComponent from "components/image/LazyImageComponent";

type ComponentProps = {
  rv: IRV;
};

const RVItem = ({ rv }: ComponentProps) => {
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const imageUrl = useMemo(() => IMAGE_CDN + rv.Photos[0].Path, []);
  return (
    <Wrap>
      <LazyImageComponent src={imageUrl} alt={""} />
      <RVName>{rv.RVName}</RVName>
      <RVPrice>{rv.AveragePrice}</RVPrice>
    </Wrap>
  );
};

export default RVItem;

const Wrap = styled.div`
  max-width: 300px;
  display: flex;
  flex-direction: column;
`;

const RVName = styled.div`
  ${typography.title};
  text-overflow: ellipsis;
`;
const RVPrice = styled.div`
  ${typography.body};
`;
