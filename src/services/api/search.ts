import apiService from "services/api/apiService";

const { fetchApi } = apiService();
const searchApi = {
  unifiedSearch: data => fetchApi("/unified-search", true, { params: data }),
};

export default searchApi;
