import axiosInstance, { getToken as getAuth } from "services/api";

const cacheData = {};
let controller = new AbortController();

const apiService = () => {
  const fetchApi = async (url, cache = false, params = {}) => {
    const key = `${url}-get-${JSON.stringify(params)}`;
    params = { ...params, signal: controller.signal };
    if (!cacheData[key]) {
      const result = await axiosInstance.get(url, params);
      if (cache) {
        cacheData[key] = result;
      }
      return result;
    }
    return cacheData[key];
  };

  const postApi = async (url, data) => {
    return axiosInstance.post(url, data, {});
  };

  const putApi = async (url, data) => {
    return axiosInstance.put(url, data, {});
  };

  const deleteApi = async (url, data?) => {
    return axiosInstance.delete(url, { data });
  };

  const cancelRequests = () => {
    controller.abort();
    controller = new AbortController();
  };

  return { fetchApi, postApi, putApi, deleteApi, getAuth, cancelRequests };
};

export default apiService;
