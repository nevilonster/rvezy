import { NO_INTERNET_CONNECTION_MESSAGE } from "services/internetConnection/const";

export const isOnline = () =>
  typeof navigator !== "undefined" && typeof navigator.onLine === "boolean"
    ? navigator.onLine
    : false;

export const NO_INTERNET_CONNECTION_ERROR = new Error(NO_INTERNET_CONNECTION_MESSAGE);
