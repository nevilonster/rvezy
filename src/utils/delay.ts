const delay = sleep =>
  new Promise<void>(resolve => {
    setTimeout(() => resolve(), sleep);
  });
export default delay;
