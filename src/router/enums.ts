export enum ROUTES {
  DASHBOARD = "/",
  NOT_FOUND = "*",
}

export const IMAGE_CDN = process.env.REACT_APP_IMAGE_URL;
export const TOKEN_KEY = "access_token";
