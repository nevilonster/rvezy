import React, { Suspense } from "react";
import { Route, Routes } from "react-router-dom";
import { IRoute } from "router/Configuration";
import Layout from "components/layout/Layout";

interface IProps {
  routes: IRoute[];
}

const Router: React.FC<IProps> = ({ routes }) => {
  return (
    <Routes>
      {routes.map((route, index) => {
        return (
          <Route
            path={route.path}
            key={index}
            element={
              <Suspense fallback={route.fallback}>
                <Layout>{route.element}</Layout>
              </Suspense>
            }
          />
        );
      })}
    </Routes>
  );
};

export default Router;
