import { IRV, IRVListData } from "model/data/types";

class ListRVS {
  private _id: string;
  private _totalRVs: number;
  private _listRVs: IRV[];

  constructor(data: IRVListData) {
    this._id = data.Id;
    this._totalRVs = data.TotalRVs;
    this._listRVs = data.ListRVs;
  }

  get totalRVs(): number {
    return this._totalRVs;
  }

  get listRVs(): IRV[] {
    return this._listRVs;
  }
}

export default ListRVS;
