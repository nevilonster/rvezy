import SearchModel from "model/search/SearchModel";
import searchApi from "services/api/search";
import { BehaviorSubject } from "rxjs";
import ListRVS from "model/data/ListRVS";
import { IFilter } from "model/search/consts";

class RVModel {
  private _featuredRVs: ListRVS;
  private _popularRVs: ListRVS;
  private _searchModel: SearchModel;
  private _isLoading: BehaviorSubject<boolean>;

  constructor() {
    console.warn("RVS model was initialized");
    this._searchModel = new SearchModel();
    this._isLoading = new BehaviorSubject<boolean>(false);
  }

  public async loadRVS(): Promise<void> {
    this._isLoading.next(true);
    console.warn("loadRVS", this._searchModel.dataForRequest);
    const response = await searchApi.unifiedSearch(this._searchModel.dataForRequest);
    if (response) {
      this._popularRVs = new ListRVS(response.data.PopularRVs);
      this._featuredRVs = new ListRVS(response.data.FeaturedRVs);
      this._isLoading.next(false);
    }
  }

  public nextPage = () => {
    const countPages = Math.round(this._popularRVs.totalRVs / this._searchModel.CountInPage);
    if (this._searchModel.CurrentPage >= countPages + 1) {
      return false;
    }
    this._searchModel.nextPage();
    this.loadRVS();
  };

  public prevPage = () => {
    if (this._searchModel.CurrentPage === 0) {
      return false;
    }
    this._searchModel.prevPage();
    this.loadRVS();
  };

  public get isLoading(): BehaviorSubject<boolean> {
    return this._isLoading;
  }

  get popularRVs(): ListRVS {
    return this._popularRVs;
  }
  get featuredRVs(): ListRVS {
    return this._featuredRVs;
  }

  setFilters(selectedFilters: Array<IFilter>) {
    this._searchModel.setFilters(selectedFilters);
  }

  get mapPosition() {
    return { lat: this._searchModel.SearchLat, lng: this._searchModel.SearchLng };
  }

  setMapPosition(lng: number, lat: number) {
    if (lng === this.mapPosition.lng && lat === this.mapPosition.lat) {
      return;
    }
    this._searchModel.setCenter(lng, lat);
    this.loadRVS();
  }
}

export default RVModel;
