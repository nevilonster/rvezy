export interface IPhoto {
  Id: number;
  Path: string;
  Description: string | null;
  Order: number;
  RVId: string;
}

export interface IRV {
  AliasName: string;
  Id: string;
  AverageRating: number;
  NumberOfReview: number;
  RVName: string;
  Guests: number;
  Distance: number;
  City: string;
  Latitude: number;
  Longitude: number;
  Photos: IPhoto[];
  State: string;
  DefaultPrice: number;
  LowSeasonNight: number;
  AveragePrice: number;
  PreDiscountedTripPrice: number | null;
  PreDiscountedAverageNightlyPrice: number;
  DiscountedTripPrice: number | null;
  DiscountedAverageNightlyPrice: number;
  DiscountPercent: number;
  HasDelivery: boolean;
  DeliveryDistanceKM: number;
  InstabookOwnerOptedIn: boolean;
  RVNumber: number;
  Description: string | null;
  PostCode: string | null;
  Year: number | null;
  Make: string | null;
  Model: string | null;
  RVType: string;
  Weight: number | null;
  OwnerId: number;
  IsFeatured: boolean;
  Country: string;
  AvailableDates: string[];
  Timezone: string;
  IsShortStay: boolean;
}

export interface IRVListData {
  Id: string;
  TotalRVs: number;
  ListRVs: IRV[];
}
