import { IFilter } from "model/search/consts";

class SearchModel {
  SearchLat: number;
  SearchLng: number;
  Country: string;
  SortOrder: string;
  CurrentPage: number;
  FeaturedCurrentPage: number;
  FeaturedPageSize: number;
  IncludeFeatured: boolean;
  CountInPage: number;
  private _filters: IFilter[];

  constructor() {
    this.CountInPage = 30;
    this.SearchLat = 45.41539;
    this.SearchLng = -75.68938;
    this.Country = "CA";
    this.SortOrder = "Recommended";
    this.CurrentPage = 0;
    this.FeaturedCurrentPage = 0;
    this.FeaturedPageSize = 5;
    this.IncludeFeatured = true;
    this._filters = [];
  }

  get dataForRequest() {
    return {
      SearchLat: this.SearchLat,
      SearchLng: this.SearchLng,
      Country: this.Country,
      SortOrder: this.SortOrder,
      CurrentPage: this.CurrentPage,
      FeaturedCurrentPage: this.FeaturedCurrentPage,
      FeaturedPageSize: this.FeaturedPageSize,
      IncludeFeatured: this.IncludeFeatured,
      ...this.getFiltersRequest(),
    };
  }
  // TODO check filters if needed to be sent to server
  private checkFilters(filters: Array<IFilter>): boolean {
    for (let i = 0; i < this._filters.length; i++) {
      const filter = this._filters[i];
      const filterIndex = filters.findIndex(f => f.value === filter.value);
      if (filterIndex === -1) {
        return false;
      }
    }
    return true;
  }

  private getFiltersRequest() {
    return this._filters.reduce((acc, f) => {
      acc[f.value] = true;
      return acc;
    }, {});
  }

  setCenter(lng: number, lat: number) {
    this.SearchLat = lat;
    this.SearchLng = lng;
    this.CurrentPage = 0;
  }

  setFilters(filters: Array<IFilter>) {
    this._filters = filters;
    this.CurrentPage = 0;
  }

  nextPage() {
    this.CurrentPage++;
  }

  prevPage() {
    this.CurrentPage--;
  }
}

export default SearchModel;
