export type IFilter = {
  label: string;
  value: string;
};
export type IFilterGroup = {
  name: string;
  filters: Array<IFilter>;
};

export const availableFilters: IFilterGroup[] = [
  {
    name: "Drivable",
    filters: [
      { label: "Class A", value: "ClassA" },
      { label: "Class B", value: "ClassB" },
      { label: "Class C", value: "ClassC" },
      { label: "Truck Camper", value: "TruckCamper" },
      { label: "Campervan", value: "Campervan" },
    ],
  },
  {
    name: "Towable",
    filters: [
      { label: "Fifth Wheel", value: "FifthWheel" },
      { label: "Sub Travel Trailer", value: "SubTravelTrailer" },
      { label: "Tent Trailer", value: "TentTrailer" },
      { label: "Hybrid", value: "Hybrid" },
      { label: "Toy Hauler", value: "ToyHauler" },
      { label: "Micro Trailer", value: "MicroTrailer" },
    ],
  },
];
