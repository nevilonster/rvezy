import { useContext } from "react";
import { RVContext } from "model/context/RVContext";

const useRV = () => {
  const { rvModel } = useContext(RVContext);
  return rvModel;
};

export default useRV;
