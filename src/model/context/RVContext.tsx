import React, { createContext } from "react";
import RVModel from "model/data/RVModel";

type ComponentProps = {
  children: JSX.Element;
};

type BaseRVContext = {
  rvModel: RVModel | null;
};

const rvModel: RVModel = new RVModel();
const RVProvider = ({ children }: ComponentProps) => {
  return <RVContext.Provider value={{ rvModel }}>{children}</RVContext.Provider>;
};

export const RVContext = createContext<BaseRVContext | null>(null);

export default RVProvider;
