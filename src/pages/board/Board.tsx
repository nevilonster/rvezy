import { useEffect, useState } from "react";
import styled from "styled-components";
import { StyledButton } from "globalStyles/theme/buttons";
import { ContentWrap } from "globalStyles/pages";
import apiService from "services/api/apiService";
import GlobalLoader from "components/loader/GlobalLoader";
import useRV from "model/context/useRV";
import RVSComponent from "components/rvs/RVSComponent";
import { availableFilters, IFilter } from "model/search/consts";
import MultiSelect, { IOption } from "components/multiSelect/MultiSelect";

const Board = () => {
  const [isReady, setIsReady] = useState(false);

  const rvModel = useRV();
  useEffect(() => {
    const subscribe = rvModel.isLoading.subscribe(isLoading => {
      setIsReady(!isLoading);
    });
    rvModel.loadRVS();
    return () => {
      subscribe.unsubscribe();

      const { cancelRequests } = apiService();
      cancelRequests();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onNext = () => {
    rvModel.nextPage();
  };

  const onPrev = () => {
    rvModel.prevPage();
  };

  const [selectedFilters, setSelectedFilters] = useState(
    availableFilters.reduce((acc, filter) => {
      acc[filter.name] = [];
      return acc;
    }, {})
  );

  const onChange = (name: string, values: IOption[]) => {
    setSelectedFilters(prev => {
      return {
        ...prev,
        [name]: [...values],
      };
    });
  };

  const onApply = () => {
    const items = Object.values(selectedFilters).reduce(
      (acc: Array<IOption>, item: Array<IOption>) => {
        acc.push(...item);
        return acc;
      },
      [] as IOption[]
    ) as Array<IFilter>;
    rvModel.setFilters(items);
    rvModel.loadRVS();
  };

  return (
    <ContentWrap>
      <ButtonContainerWrap>
        {availableFilters.map(filter => {
          return (
            <MultiSelect
              isMulti={true}
              value={selectedFilters[filter.name]}
              onChange={values => onChange(filter.name, values)}
              options={filter.filters}
              key={filter.name}
            />
          );
        })}
        <StyledButton onClick={onApply}>Apply</StyledButton>
        <StyledButton onClick={onPrev}>Last page</StyledButton>
        <StyledButton onClick={onNext}>Next page</StyledButton>
      </ButtonContainerWrap>
      {isReady && <RVSComponent rvsList={rvModel.popularRVs} />}
      {!isReady && <GlobalLoader />}
    </ContentWrap>
  );
};

export default Board;

const ButtonContainerWrap = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  align-self: center;
  gap: 1rem;
`;
